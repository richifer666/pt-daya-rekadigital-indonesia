const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');
 
const should = chai.should();
 
chai.use(chaiHttp);

// describe("Search Customers", () => {
//   describe("GET /api/customers/", () => {
//     it("should get all transactions", (done) => {
//       chai.request(app)
//         .get('/api/customers')
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.body.should.be.a('object');
//           done();
//         });
//     });
//     it("should get a single customers", (done) => {
//       const id = 1;
//       chai.request(app)
//         .get(`/api/customers/${id}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.body.should.be.a('object');
//           done();
//         });
//     });

//     it("should not get a single todo", (done) => {
//       const id = 10;
//       chai.request(app)
//         .get(`/api/customers/${id}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           done();
//         });
//     });
//   });
// });

describe("1. Insert Transactions", () => {
  describe("GET /api/transactions/store", () => {
    it("should add transactions on /store POST", (done) => {
      chai.request(app)
        .post('/api/transactions/store')
        .send({
          customer_id: 1,
          menu: 'Mie Ayam',
          price: 10000,
          qty: 5,
          payment: "cash",
          total: 50000,
        })
        .end((err, res) => {
          res.should.have.status(201);
          res.should.be.json;
          done();
        });
    });
  });
});

describe("2. Search Transactions", () => {
  describe("GET /api/transactions/search", () => {
    it("a. should get all transactions", (done) => {
      chai.request(app)
        .get('/api/transactions/search')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });

    it("b. should get all transactions filtered by menu name", (done) => {
      chai.request(app)
        .get('/api/transactions/search')
        .send({
          menu: "Mie Ayam",
          price: undefined
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          for(i=0;i<res.body.data.length;i++){
            res.body.data[i].menu.should.equal('Mie Ayam');
          }
          done();
        });
    });

    it("c. should get all transactions filtered by price", (done) => {
      chai.request(app)
        .get('/api/transactions/search')
        .send({
          menu: undefined,
          price: 8000,
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          for(i=0;i<res.body.data.length;i++){
            res.body.data[i].price.should.equal(8000);
          }
          done();
        });
    });

    it("d. should get all transactions filtered by menu name and price", (done) => {
      chai.request(app)
        .get('/api/transactions/search')
        .send({
          menu: "Mie Ayam",
          price: 10000,
        })
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          for(i=0;i<res.body.data.length;i++){
            res.body.data[i].menu.should.equal('Mie Ayam');
            res.body.data[i].price.should.equal(10000);
          }
          done();
        });
    });
  });
});