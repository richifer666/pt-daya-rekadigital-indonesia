1. Bagaimana jika terdapat ribuan transaksi pada database?
2. Bagaimana jika terdapat banyak user yang mengakses API tersebut secara
bersamaan?

Jawab :
Dari kedua pertanyaan ini dapat dijawab bersamaan yaitu dengan cara :
1. Melakukan Scalling Pada Server Database
2. Membuat Cache Database Dengan Redis
3. Membuat Mekanisme Lock Secara Distribusi Dengan Redis Agar Tidak Terjadi Race Condition
4. Untuk Menanggulangi Data Yang Ditampilkan Dalam Pencarian / GET PLP (Page List Product) DATA Dapat Menggunakan Pagination Dan Melimitasi Skip Pada Pagination Serta Menggunakan Filter Berdasrakan Beberapa Paramater Seperti Rentang Harga, Rentang Waktu, dkk


Alasan saya memilih tech stack ini "express js / node js" karena didalam ketentuan harus memilih antara NodeJS atau GOLANG, menurut saya lebih familier menggunakan NodeJS, kemudian framework yang digunakan memilih expressJS Karena lebih familiar dan populer, dan menggunakan database MySQL karena saya terbiasa menggunakan RAW Query dari MySQL 

Saya belum pernah menggunakan docker-compose
