/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100418
 Source Host           : localhost:3306
 Source Schema         : express_sandbox

 Target Server Type    : MySQL
 Target Server Version : 100418
 File Encoding         : 65001

 Date: 21/01/2024 10:11:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 'Santosa');
INSERT INTO `customers` VALUES (2, 'Budi');
INSERT INTO `customers` VALUES (3, 'Lulu');
INSERT INTO `customers` VALUES (4, 'Andi');
INSERT INTO `customers` VALUES (5, 'AA');

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NULL DEFAULT NULL,
  `menu` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `price` int NULL DEFAULT NULL,
  `qty` int NULL DEFAULT NULL,
  `payment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `total` int NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `customer_id`(`customer_id`) USING BTREE,
  CONSTRAINT `Transaction` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES (15, 1, 'Mie Ayam', 10000, 5, 'cash', 10000, '2024-01-20 16:35:00');
INSERT INTO `transactions` VALUES (16, 2, 'Mie Ayam', 10000, 10, 'cash', 100000, '2024-01-20 16:36:34');
INSERT INTO `transactions` VALUES (17, 1, 'Mie Ayam', 8000, 10, 'cash', 100000, '2024-01-20 16:36:39');
INSERT INTO `transactions` VALUES (18, 5, 'Bakso', 8000, 5, 'cash', 5, '2024-01-20 16:57:47');
INSERT INTO `transactions` VALUES (19, 5, 'Tempe', 500, 15, 'cash', 15, '2024-01-20 16:58:00');
INSERT INTO `transactions` VALUES (20, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:27:24');
INSERT INTO `transactions` VALUES (21, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:29:04');
INSERT INTO `transactions` VALUES (22, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:29:44');
INSERT INTO `transactions` VALUES (23, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:29:57');
INSERT INTO `transactions` VALUES (24, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:30:10');
INSERT INTO `transactions` VALUES (25, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:30:21');
INSERT INTO `transactions` VALUES (26, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:30:32');
INSERT INTO `transactions` VALUES (27, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:35:28');
INSERT INTO `transactions` VALUES (28, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:35:55');
INSERT INTO `transactions` VALUES (29, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:37:48');
INSERT INTO `transactions` VALUES (30, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:38:06');
INSERT INTO `transactions` VALUES (31, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:39:09');
INSERT INTO `transactions` VALUES (32, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:39:32');
INSERT INTO `transactions` VALUES (33, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:40:04');
INSERT INTO `transactions` VALUES (34, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:40:40');
INSERT INTO `transactions` VALUES (35, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:40:49');
INSERT INTO `transactions` VALUES (36, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:42:00');
INSERT INTO `transactions` VALUES (37, 1, 'Mie Ayam', 8000, 5, 'cash', 5, '2024-01-21 09:43:48');
INSERT INTO `transactions` VALUES (38, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:44:33');
INSERT INTO `transactions` VALUES (39, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:44:48');
INSERT INTO `transactions` VALUES (40, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:45:21');
INSERT INTO `transactions` VALUES (41, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:47:01');
INSERT INTO `transactions` VALUES (42, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:47:13');
INSERT INTO `transactions` VALUES (43, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:47:32');
INSERT INTO `transactions` VALUES (44, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:49:42');
INSERT INTO `transactions` VALUES (45, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:50:04');
INSERT INTO `transactions` VALUES (46, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:50:17');
INSERT INTO `transactions` VALUES (47, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:51:46');
INSERT INTO `transactions` VALUES (48, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:52:01');
INSERT INTO `transactions` VALUES (49, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:52:34');
INSERT INTO `transactions` VALUES (50, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:52:45');
INSERT INTO `transactions` VALUES (51, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:53:03');
INSERT INTO `transactions` VALUES (52, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:53:55');
INSERT INTO `transactions` VALUES (53, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:54:23');
INSERT INTO `transactions` VALUES (54, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:54:58');
INSERT INTO `transactions` VALUES (55, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:55:48');
INSERT INTO `transactions` VALUES (56, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:56:57');
INSERT INTO `transactions` VALUES (57, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:57:49');
INSERT INTO `transactions` VALUES (58, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:59:38');
INSERT INTO `transactions` VALUES (59, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 09:59:43');
INSERT INTO `transactions` VALUES (60, 1, 'Mie Ayam', 8000, 5, 'qris', 40000, '2024-01-21 09:59:54');
INSERT INTO `transactions` VALUES (61, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:01:48');
INSERT INTO `transactions` VALUES (62, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:02:02');
INSERT INTO `transactions` VALUES (63, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:02:14');
INSERT INTO `transactions` VALUES (64, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:03:31');
INSERT INTO `transactions` VALUES (65, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:04:55');
INSERT INTO `transactions` VALUES (66, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:06:39');
INSERT INTO `transactions` VALUES (67, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:07:13');
INSERT INTO `transactions` VALUES (68, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:08:39');
INSERT INTO `transactions` VALUES (69, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:08:57');
INSERT INTO `transactions` VALUES (70, 1, 'Mie Ayam', 10000, 5, 'cash', 5, '2024-01-21 10:10:30');

SET FOREIGN_KEY_CHECKS = 1;
