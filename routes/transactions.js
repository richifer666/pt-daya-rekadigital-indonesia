const express = require('express');
const router = express.Router();

//import express validator
const { body, validationResult } = require('express-validator');

//import database
const connection = require('../config/database');

/**
 * STORE TRANSACTION
 */
router.post('/store', [

    //validation
    body('customer_id').notEmpty(),
    body('menu').notEmpty(),
    body('price').notEmpty(),
    body('qty').notEmpty(),
    body('payment').notEmpty()

], (req, res) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({
            errors: errors.array()
        });
    }

    //define formData
    let formData = {
        customer_id: req.body.customer_id,
        menu: req.body.menu,
        price: req.body.price,
        qty: req.body.qty,
        payment: req.body.payment,
        total: req.body.qty,
        created_at: new Date()
    }

    // insert query
    connection.query('INSERT INTO transactions SET ?', formData, function (err, rows) {
        //if(err) throw err
        if (err) {
            return res.status(500).json({
                status: false,
                message: 'Internal Server Error',
            })
        } else {
            return res.status(201).json({
                status: true,
                message: 'Insert Data Successfully',
                data: rows[0]
            })
        }
    })

});

/**
 * INDEX POSTS
 */
router.get('/search', function (req, res) {
    
    //define formData
    let formData = {
        menu: req.body.menu,
        price: req.body.price,
    }

    
    console.log(formData.menu,formData.price);

    const base_query = `SELECT * FROM transactions INNER JOIN customers ON transactions.customer_id = customers.id `;
    let query = '';
    if(formData.menu !== undefined && formData.price == undefined){
        query = `WHERE transactions.menu LIKE  "%${formData.menu}%"`;
        console.log('Menu doang');
    }

    if(formData.menu == undefined && formData.price !== undefined){
        query = `WHERE transactions.price LIKE "%${formData.price}%"`;
        console.log("Harga Doang");
    }

    if(formData.price !== undefined && formData.menu !== undefined){
        query = `WHERE transactions.menu LIKE "%${formData.menu}%" AND transactions.price LIKE "%${formData.price}%"`;
        console.log("Ada Harga Ada Menu");
     }

     //query
     connection.query(base_query+query+` ORDER BY customers.name ASC`, function (err, rows) {
        if (err) {
            return res.status(500).json({
                status: false,
                message: 'Internal Server Error',
            })
        } else {
            return res.status(200).json({
                status: true,
                message: 'List Data Transactions',
                data: rows
            })
        }
    });
    
});

module.exports = router;